## Скрипт Bash для запуска Llama `start.sh`

Этот файл содержит инструкции по использованию скрипта bash,
предназначенного для запуска Llama с использованием torchrun.

### Использование скрипта

Скрипт поддерживает несколько командных аргументов:

- `-m, --model`: Имя используемой модели
- `-p, --processors`: Количество используемых процессоров (по умолчанию `1`)
- `-s, --sequence`: Максимальная длина последовательности (по умолчанию `1024`)
- `-b, --batch`: Максимальный размер пакета (по умолчанию `1`)
- `-d, --models-dir`: Путь к директории с моделями (по умолчанию `./models`)
- `-r, --run-file`: Путь к файлу, который должен быть запущен (по умолчанию `./llama-cpu/example.py`)
- `-t, --torchrun-args`: Дополнительные аргументы для torchrun
- `-l, --log-file`: Файл для логирования (по умолчанию `torchrun.log`)

### Примеры использования скрипта

#### Запуск скрипта без параметров

Самый базовый способ запустить скрипт - это вызвать его без параметров.
В этом случае будут использованы значения по умолчанию.

```bash
./start.sh
```

#### Указание файла запуска

Вы можете указать файл для запуска как первый неопциональный аргумент.
Если такой аргумент присутствует, скрипт будет использовать его вместо файла по умолчанию.

```bash
./start.sh /home/user/script.py
```

В этом примере скрипт будет использовать `/home/user/script.py`
как файл для запуска, вместо файла по умолчанию.

#### Указание модели

Вы можете указать модель с помощью параметра `-m` или `--model`.

```bash
./start.sh -m model_name
```

#### Указание количества процессоров и размера пакета

Вы можете указать количество используемых процессоров с помощью параметра
`-p` или `--processors`, а также размер пакета с помощью параметра `-b` или `--batch`.

```bash
./start.sh -m model_name -p 4 -b 16
```

#### Указание пути к директории с моделями и файлу для запуска

Вы можете указать путь к директории с моделями с помощью параметра
`-d` или `--models-dir`, и путь к файлу, который должен быть запущен, с помощью параметра
`-r` или `--run-file`.

```bash
./start.sh -d /path/to/models/dir -r /path/to/run/file.py
```

#### Полный набор параметров

Вы можете указать все параметры, чтобы полностью настроить запуск скрипта.

```bash
./start.sh -m model_name -p 4 -s 512 -b 16 \
  -d /path/to/models/dir \
  -r /path/to/run/file.py \
  -t "--arg1 value1 --arg2 value2" \
  -l /path/to/log/file.log
```

При вызове со всеми параметрами, каждый из них будет учитываться при запуске скрипта.
Если какой-то из параметров не указан, будет использоваться значение по умолчанию.

### Структура файлов:

```bash
├── download.sh
├── llama-cpu
│   ├── CODE_OF_CONDUCT.md
│   ├── CONTRIBUTING.md
│   ├── LICENSE
│   ├── MODEL_CARD.md
│   ├── README.md
│   ├── build
│   │   └── lib
│   │       └── llama
│   │           ├── __init__.py
│   │           ├── generation.py
│   │           ├── model.py
│   │           └── tokenizer.py
│   ├── download.sh
│   ├── example.py
│   ├── llama
│   │   ├── __init__.py
│   │   ├── __pycache__
│   │   │   ├── __init__.cpython-311.pyc
│   │   │   ├── generation.cpython-311.pyc
│   │   │   ├── model.cpython-311.pyc
│   │   │   └── tokenizer.cpython-311.pyc
│   │   ├── generation.py
│   │   ├── model.py
│   │   └── tokenizer.py
│   ├── llama.egg-info
│   │   ├── PKG-INFO
│   │   ├── SOURCES.txt
│   │   ├── dependency_links.txt
│   │   └── top_level.txt
│   ├── requirements.txt
│   └── setup.py
├── models
│   ├── llama-2-70b-chat
│   │   ├── llama-2-70b-chat
│   │   │   ├── checklist.chk
│   │   │   ├── consolidated.00.pth
│   │   │   ├── consolidated.01.pth
│   │   │   ├── consolidated.02.pth
│   │   │   ├── consolidated.03.pth
│   │   │   ├── consolidated.04.pth
│   │   │   ├── consolidated.05.pth
│   │   │   ├── consolidated.06.pth
│   │   │   ├── consolidated.07.pth
│   │   │   └── params.json
│   │   ├── tokenizer.model
│   │   └── tokenizer_checklist.chk
│   └── llama-2-7b-chat
│       ├── llama-2-7b-chat
│       │   ├── checklist.chk
│       │   ├── consolidated.00.pth
│       │   └── params.json
│       ├── tokenizer.model
│       └── tokenizer_checklist.chk
├── readme.md
├── start.sh
└── torchrun.log
```
