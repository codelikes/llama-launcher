#!/bin/bash

# Получить имя скрипта
SCRIPT_NAME=$(basename "$0")

# Путь к директории моделей
DEFAULT_MODELS_DIR=$(readlink -f $(dirname "$0"))/models

# Путь к файлу, который должен быть запущен
DEFAULT_RUN_FILE=$(readlink -f $(dirname "$0"))/llama-cpu/example.py

# Параметры по умолчанию
PROCESSORS=1
SEQUENCE=1024
BATCH=1
LOG_FILE="torchrun.log"

# Проверить первый аргумент на наличие пути к файлу
if [[ "$1" != -* && -n "$1" ]]; then
  RUN_FILE=$(readlink -f "$1")
  shift
fi

# Цикл обработки аргументов командной строки
while (("$#")); do
  case "$1" in
  -m | --model)
    MODEL="$2"
    shift 2
    ;;
  -p | --processors)
    PROCESSORS="$2"
    shift 2
    ;;
  -s | --sequence)
    SEQUENCE="$2"
    shift 2
    ;;
  -b | --batch)
    BATCH="$2"
    shift 2
    ;;
  -d | --models-dir)
    MODELS_DIR=$(readlink -f "$2")
    shift 2
    ;;
  -r | --run-file)
    RUN_FILE=$(readlink -f "$2")
    shift 2
    ;;
  -t | --torchrun-args)
    TORCHRUN_ARGS="$2"
    shift 2
    ;;
  -l | --log-file)
    LOG_FILE=$(readlink -f "$2")
    shift 2
    ;;
  -h | --help)
    echo "Использование: $SCRIPT_NAME [опции]"
    echo
    echo "Опции:"
    echo "-m, --model <имя модели>            имя используемой модели"
    echo "-p, --processors <процессоры>      количество используемых процессоров (по умолчанию 1)"
    echo "-s, --sequence <последовательность> максимальная длина последовательности (по умолчанию 1024)"
    echo "-b, --batch <пакет>                максимальный размер пакета (по умолчанию 1)"
    echo "-d, --models-dir <dir>             путь к директории с моделями (по умолчанию ./models)"
    echo "-r, --run-file <file>              путь к файлу, который должен быть запущен (по умолчанию ./llama-cpu/example.py)"
    echo "-t, --torchrun-args <args>         дополнительные аргументы для torchrun"
    echo "-l, --log-file <файл>              файл для логирования (по умолчанию torchrun.log)"
    exit 0
    ;;
  *)
    echo "$SCRIPT_NAME: ошибка: неправильный аргумент — $1"
    echo "См. '$SCRIPT_NAME --help'."
    exit 1
    ;;
  esac
done

MODELS_DIR=${MODELS_DIR:-$DEFAULT_MODELS_DIR}
RUN_FILE=${RUN_FILE:-$DEFAULT_RUN_FILE}

# Если модель не была указана, предложить пользователю выбрать модель
if [ -z "$MODEL" ]; then
  echo "Выберите модель:"
  select MODEL in $(ls "$MODELS_DIR"); do
    if [ -n "$MODEL" ]; then
      break
    else
      echo "Неверный ввод. Пожалуйста, попробуйте снова."
    fi
  done
fi

# Запуск Llama и запись вывода в файл
torchrun --nproc_per_node "$PROCESSORS" "$RUN_FILE" \
  --ckpt_dir "$MODELS_DIR/$MODEL/$MODEL/" \
  --tokenizer_path "$MODELS_DIR/$MODEL/tokenizer.model" \
  --max_seq_len "$SEQUENCE" --max_batch_size "$BATCH" \
  $TORCHRUN_ARGS 2>&1 | tee -a "$LOG_FILE"
